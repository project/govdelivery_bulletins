# GovDelivery Bulletins

This module provides a set of utilities to have your Drupal powered site use GovDelivery's Communications Cloud API - Bulletins resource https://developer.govdelivery.com/api/comm_cloud_v1/Default.htm#API/Comm%20Cloud%20V1/API_CommCloudV1_Bulletins.htm to create and send Bulletin notifications including an external trigger endpoint.  This module supports custom code a developer must create to send the bulletin.  It does not create them by itself.

It is more focused in function than the GovDelivery Integration module https://www.drupal.org/project/govdelivery.


## FAQs

### During initial setup and testing, how do I make sure I am not sending Bulletins?
The two checkboxes in the "Basic operations" section of the admin should start
out being disabled.  With them off, Bulletins are not queued and the queue is
not processed.  **Caution** Only turn them both on when you are ready to send.

### What should I use for the GovDelivery API endpoint setting?
There are a few styles of endpoint definitions.
   - Staging - Useful for initial testing and you may want to set up your
     lower environments to point to it.
      - Send immediately: https://stage-api.govdelivery.com/api/account/ACCOUNT_CODE/bulletins/send_now
      - Create draft Bulletin: https://stage-api.govdelivery.com/api/account/ACCOUNT_CODE/bulletins.xml
   - Production
      - Send immediately: https://api.govdelivery.com/api/account/ACCOUNT_CODE/bulletins/send_now
      - Create draft Bulletin: https://api.govdelivery.com/api/account/ACCOUNT_CODE/bulletins.xml

Your ACCOUNT_CODE can be found via the GovDelivery administrative interface.
Look in the URL.

### How do I use the service to add a bulletin to the queue?
The service to add a Bulletin to the queue can be called from anything that you
would like to have perform this [ Example: hook_entity_insert(),
hook_entity_update(), hook_entity_presave(), hook_update_N(), custom event
subscriber ...]

It is called like this:
```
\Drupal::service('govdelivery_bulletins.add_bulletin_to_queue')
        // The QueueUid can be very helpful in debugging.  It is also mandatory
        // if you are deduping the queue.  The QueueUid is what is used to
        // determine duplicates.
        ->setQueueUid('some unique string') // Optional, but mandatory if deduping.
        //  See separate FAQ.
        ->setFlag('dedupe', TRUE)  // Optional.
        ->setBody("Some body text for the message.")
        ->addCategory('sales')  // Optional.
        ->addCategory('emergency')  // Optional.
        ->setFooter("Some body text for the message.")  // Optional.
        ->setFromAddress('ADDRESS_ID') // Optional and only needs to be provided if your GovDelivery account has multiple from addresses configured.
        ->setHeader("Some text for the subject line.")  // Optional.
        ->setSubject('Some text for the email subject.')
        ->setSMSBody('Some text SMS text.')  // Optional.
        ->addTopic('ABC-123')  // Optional.
        ->addTopic('DEF-789')  // Optional.
        ->setXmlBool('click_tracking', FALSE)  // Optional.
        ->setXmlBool('open_tracking', FALSE)  // Optional.
        ->setXmlBool('publish_rss', FALSE)  // Optional.
        ->setXmlBool('share_content_enabled', FALSE)  // Optional.
        ->setXmlBool('urgent', FALSE)  // Optional.

        ->addToQueueAndReset();
```

To add a test, it would look like this.
```
\Drupal::service('govdelivery_bulletins.add_bulletin_to_queue')
        ->setFlag('test', TRUE)
        ->addEmailAddress('test.recipient1@example.org')
        ->addEmailAddress('test.recipient2@example.org')
        ->addToQueueAndReset();
```

GovDelivery Bulletins API https://developer.govdelivery.com/api/comm_cloud_v1/Default.htm#API/Comm%20Cloud%20V1/API_CommCloudV1_Bulletins_CreateandSendBulletin.htm

### How do I securely store GovDelivery API endpoint, username and password.

Should be stored in settings.local.php, like so:
```
// GovDelivery settings.
$config['govdelivery_bulletins.settings']['govdelivery_endpoint'] = 'GOVDELIVERY-API-ENDPOINT';
$config['govdelivery_bulletins.settings']['govdelivery_username'] = 'YOUR-GOVDELIVERY-USERNAME';
$config['govdelivery_bulletins.settings']['govdelivery_password'] = 'YOUR-GOVDELIVERY-PASSWORD';
```
Then to access:
```
$config = \Drupal::config('govdelivery_bulletins.settings');
$apiEndPoint = $config->get('govdelivery_endpoint');
$apiUsername = $config->get('govdelivery_username');
$apiPassword = $config->get('govdelivery_password');

```
You can also save your username and password by setting them in the GovDelivery
Bulletins Admin form /admin/config/services/govdelivery-bulletins/settings.

This is strongly discouraged, and presents a security vulnerability.

### Why does this use a queue?

* A queue makes sure that in the event of a problem reaching the GovDelivery
  endpoint, no bulletins get lost.  Anything that is unsuccessfully received by
  GovDelivery stays in the queue and is retried until the send is successful.
* A queue also makes sure that in the event of sending many messages, that
  sending them is not a blocking process that causes your site to timeout or
  become unresponsive.

### How do we trigger processing of the queue?
There are three ways to trigger the GovDelivery Bulletin queue:
1. By hand - Go to /admin/config/services/govdelivery-bulletins/queue and click
  the "Process queue" button.  This is used mainly during initial testing. It is
  not recommended as a sustainable solution.
2. Drush command 'drush queue:run govdelivery_bulletins'.  This is useful for
  testing while building your custom implementation. It is also useful for
  making the queue get processed by cron or any other task runner that can run
  Drush commands.
3. GET request to external endpoint - Making a GET request to
`https://example.com/api/govdelivery_bulletins/queue?EndTime={timestamp that
anything before this should be sent}` can be used to trigger the queue from an
external tool that does not have access to run drush commands on the site.

### Why is the queue list so hard to read?
The queue list /admin/config/services/govdelivery-bulletins/queue is not meant
for everyone.  It is mainly for developers to be able to see why sending a
specifc item is failing.  The queue item shows the API payload in the exact form
it is sent, so that nothing gets hidden or covered up.  It is mainly a debugging
tool, or a quick way to monitor that the queue is running.

### What is deduping the queue `->setFlag('dedupe', TRUE)`
It is possible while you are adding many things to the queue, that you will end
with duplicate Bulletins.  Generally, people don't like receiving duplicate
emails.  This flag allows the queue to not accepet duplicates.  Usually, you
want this deduping to be on.  When using deduping, you must set the QueueUid.
Deduping can be a very expensive/slow process if you have a lot of items queued
and you are trying to add more.  It is slow because in order to add one item,
it has to open each queue item to see if it matches the QueueUid it is trying to
add.

### Can I see an example of a site implementing Govdelivery Bulletins?
Here is a custom module in place at the Department of Veterans Affairs
https://github.com/department-of-veterans-affairs/va.gov-cms/tree/main/docroot/modules/custom/va_gov_govdelivery

### When I have errors, I see the exception, why is it too truncate?
This limitation comes from Guzzle.  The only workaround I have found is to
check the php logs and find the actual error. Another option is to make the same
request directly (not using Drupal) and see the actual result. Postman can be
very handy for this.
