<?php

namespace Drupal\govdelivery_bulletins\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GovDeliveryBulletinsAdminForm is used for the module settings.
 */
class GovDeliveryBulletinsAdminForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gov_delivery_bulletins_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('govdelivery_bulletins.settings');

    $form['circuit-breakers'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Basic operations'),
    ];
    $into_text = $this->t('Bulletins to be sent are first added to a queue, and then the queue is processed.'). '</br>';
    $into_text .= $this->t('When the queue item is processed, it is sent to GovDelivery'). '</br>';
    $into_text .= $this->t('To be fully operational, both of these must be enabled.');

    $form['circuit-breakers']['introduction'] = [
      'field_label' => [
        '#type' => 'markup',
        '#markup' => $into_text,
      ],
    ];

    $form['circuit-breakers']['enable_bulletin_queuing'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable bulletin queuing.'),
      '#description' => $this->t('Add Bulletins to the queue when the service AddBulletinToQueue() is called. If disabled, it will still write the fake Bulletin addition to the Drupal log, which is useful for testing.'),
      '#weight' => '0',
      '#default_value' => $config->get('enable_bulletin_queuing'),
    ];

    $form['circuit-breakers']['enable_bulletin_queue_sends_to_govdelivery'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable bulletin queue sending to GovDelivery API.'),
      '#description' => $this->t('Enable the queue processor to post to the GovDelivery Bulletin API. If disabled, will not actually send, but will still write to log, which is useful for testing.'),
      '#weight' => '2',
      '#default_value' => $config->get('enable_bulletin_queue_sends_to_govdelivery'),
    ];

    $account_info_description = '<strong>' . $this->t('WARNING:') . '</strong>';
    $account_info_description .= $this->t(
      'These fields store GovDelivery account info in the database. - THIS IS NOT RECOMMENDED.
      Preferred method is to store in settings.local.php and use environment variables - see README for instructions.');
    $form['account-info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('GovDelivery Account Information'),
      '#description' => $account_info_description,
    ];

    $account_description = $this->t('The endpoint ususually resembles this pattern:');
    $account_description .= '</br> https://api.govdelivery.com/api/account/ACCOUNT_CODE/bulletins/send_now';
    $account_description .= '</br>' . $this->t('Your ACCOUNT_CODE can be found via the GovDelivery administrative interface. Look in the URL.');
    $form['account-info']['govdelivery_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GovDelivery API Endpoint'),
      '#description' => $account_description,
      '#weight' => '6',
      '#default_value' => $config->get('govdelivery_endpoint'),
    ];

    $form['account-info']['govdelivery_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GovDelivery Username'),
      '#description' => $this->t(
        'The username for the GovDelivery account that should be used.'),
      '#weight' => '6',
      '#default_value' => $config->get('govdelivery_username'),
    ];

    $form['account-info']['govdelivery_password'] = [
      '#type' => 'password',
      '#title' => $this->t('GovDelivery Password'),
      '#description' => $this->t(
        'The password for the GovDelivery account that should be use.'),
      '#weight' => '7',
      '#default_value' => $config->get('govdelivery_password'),
    ];

    $form['remote-trigger'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Remote trigger'),
      '#description' => $this->t('This enables sending items in the queue by making a GET request.  It is useful for decoupled sites whose content may lag behind Drupal.'),
    ];
    $form['remote-trigger']['api_queue_trigger_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable API Queue Trigger'),
      '#description' => $this->t(
        'Enables the ability to visit "@path" to trigger processing of the bulletin queue.',
        ['@path' => '/api/govdelivery_bulletins/queue?EndTime=[unix timestamp]']
      ),
      '#weight' => '4',
      '#default_value' => $config->get('api_queue_trigger_enabled'),
    ];

    $description = $this->t('Allows accessing the API Queue Trigger by way of basic auth.');
    $caution = $this->t('Requires @basic_auth module be enabled.', ['@basic_auth' => 'basic_auth']);
    // Check to see if basic_auth module exists.
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('basic_auth')) {
      $status = "basic_auth: <strong>{$this->t('enabled')}</strong>";
      $description = "{$description}<br/>{$caution}<br/>{$status}";
    }
    else {
      $status = "basic_auth: <strong>{$this->t('disabled')}</strong>";
      $description = "{$description}<br/><strong>{$caution}</strong><br/>{$status}";
    }
    $form['remote-trigger']['api_queue_trigger_allow_basic_auth'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow @basic_auth to be used to access API Queue Trigger', ['@basic_auth' => 'basic_auth']),
      '#description' => $description,
      '#weight' => '5',
      '#default_value' => $config->get('api_queue_trigger_allow_basic_auth'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => '20',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = self::configFactory()->getEditable('govdelivery_bulletins.settings');
    $config
      ->set('api_queue_trigger_enabled', $form_state->getValue('api_queue_trigger_enabled'))
      ->set('api_queue_trigger_allow_basic_auth', $form_state->getValue('api_queue_trigger_allow_basic_auth'))
      ->set('enable_bulletin_queuing', $form_state->getValue('enable_bulletin_queuing'))
      ->set('enable_bulletin_queue_sends_to_govdelivery', $form_state->getValue('enable_bulletin_queue_sends_to_govdelivery'))
      ->set('govdelivery_endpoint', $form_state->getValue('govdelivery_endpoint'))
      ->set('govdelivery_username', $form_state->getValue('govdelivery_username'))
      ->set('govdelivery_password', $form_state->getValue('govdelivery_password'))
      ->save();
  }

}
